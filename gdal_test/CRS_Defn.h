#pragma once
#include "gdal_priv.h"
#include "ogrsf_frmts.h"
#include <iostream>

class CRS_defn {
public:
	//添加属性列
	static void addDefn(OGRLayer* , OGRFieldDefn*);
	//给属性表某列赋值
	template <typename F>
	static void assignDefn(OGRLayer* , int& , F);
	//删除指定属性表列
	static void deleteDefn(OGRLayer*, int);
};

template <typename F>
void CRS_defn::assignDefn(OGRLayer* tarLayer, int& fieldIndex, F assignRule)
{
	if (tarLayer == NULL || fieldIndex < 0 || fieldIndex >= tarLayer->GetLayerDefn()->GetFieldCount() || assignRule == NULL) {
		std::cout << "输入无效" << std::endl;
		return;
	}
	for (const auto& oFeature : *tarLayer) {
		oFeature->SetField(fieldIndex, assignRule(oFeature.get()));
		tarLayer->SetFeature(oFeature.get());
	}
	//tarLayer->SyncToDisk();
}