#include "OGRGeometry_demo1.h"
//打开ESRI ShapeFile
void OpenESRIShp(const char* FileDIR) {
	//支持中文路径 
	CPLSetConfigOption("GDAL_FILENAME_IS_UTF8", "NO");
    //CPLSetConfigOption("SHAPE_ENCODING", "");
	//注册所有驱动
	OGRRegisterAll();
	//打开数据
	//auto *poDS = (GDALDataset*)GDALOpenEx(FileDIR, GDAL_OF_VECTOR, NULL, NULL, NULL);
    GDALDatasetUniquePtr poDS(GDALDataset::Open(FileDIR, GDAL_OF_VECTOR));
	if (poDS == NULL)
	{
		printf("Open failed.\n");
		exit(1);
	}
     

	//打开第一个图层（Layer）
	auto poLayer = poDS->GetLayer(0);
	std::cout << poLayer->GetFeatureCount() << std::endl;

    //打开第一个要素（feature）
    //auto poFeature = poLayer->GetFeature(0);
    //std::cout << poFeature->GetFID() << std::endl;

    //打开属性表，输出表头
    OGRFeatureDefn* poFDefn = poLayer->GetLayerDefn();
    std::cout << poFDefn->GetFieldCount() << std::endl;
    for (int FieldIndex = 0; FieldIndex < poFDefn->GetFieldCount(); FieldIndex++) {
        auto&& layerField0 = poFDefn->GetFieldDefn(FieldIndex);
        std::cout << layerField0->GetNameRef() << " ";
    }

	//获得每个要素的属性表
    //c++ 11 写法
    //万能引用，oField实际类型为左值引用迭代器 const OGRFeature::FiledValue&
    //也可以不判断每个oField的类型，直接GetFieldAsString
    for (const auto& poFeature : poLayer) {
        for (auto&& oField : *poFeature)
        {
            if (oField.IsUnset())
            {
                printf("(unset),");
                continue;
            }
            if (oField.IsNull())
            {
                printf("(null),");
                continue;
            }
            switch (oField.GetType())
            {
            case OFTInteger:
                printf("%d,", oField.GetInteger());
                break;
            case OFTInteger64:
                printf(CPL_FRMT_GIB ",", oField.GetInteger64());
                break;
            case OFTReal:
                printf("%.3f,", oField.GetDouble());
                break;
            case OFTString:
                // GetString() returns a C string
                printf("%s,", oField.GetString());
                break;
            default:
                // Note: we use GetAsString() and not GetString(), since
                // the later assumes the field type to be OFTString while the
                // former will do a conversion from the original type to string.
                printf("%s,", oField.GetAsString());
                break;
            }
        }
        std::cout << std::endl;
    }
    

    //旧写法
    //OGRFeatureDefn* poFDefn = poLayer->GetLayerDefn();
    //for (int iField = 0; iField < poFDefn->GetFieldCount(); iField++)
    //{
    //    if (!poFeature->IsFieldSet(iField))
    //    {
    //        printf("(unset),");
    //        continue;
    //    }
    //    if (poFeature->IsFieldNull(iField))
    //    {
    //        printf("(null),");
    //        continue;
    //    }
    //    OGRFieldDefn* poFieldDefn = poFDefn->GetFieldDefn(iField);

    //    switch (poFieldDefn->GetType())
    //    {
    //    case OFTInteger:
    //        printf("%d,", poFeature->GetFieldAsInteger(iField));
    //        break;
    //    case OFTInteger64:
    //        printf(CPL_FRMT_GIB ",", poFeature->GetFieldAsInteger64(iField));
    //        break;
    //    case OFTReal:
    //        printf("%.3f,", poFeature->GetFieldAsDouble(iField));
    //        break;
    //    case OFTString:
    //        printf("%s,", poFeature->GetFieldAsString(iField));
    //        break;
    //    default:
    //        printf("%s,", poFeature->GetFieldAsString(iField));
    //        break;
    //    }
    //}

    //读取几何图形（Geometry）
    //读取图层第一个要素（feature）
    auto poFeature = poLayer->GetFeature(0);
    OGRGeometry* poGeometry;
    poGeometry = poFeature->GetGeometryRef();
    if (poGeometry != NULL
        && wkbFlatten(poGeometry->getGeometryType()) == wkbPoint)
    {
        OGRPoint* poPoint = poGeometry->toPoint();
        printf("%.3f,%3.f\n", poPoint->getX(), poPoint->getY());
    }
    else if(poGeometry != NULL && wkbFlatten(poGeometry->getGeometryType()) == wkbPolygon)
    {
        OGRPolygon* poPolygon = poGeometry->toPolygon();
        int CurveNum = 0;
        for (auto poCurve : *poPolygon) {
            CurveNum++;
            for (auto poPoint : *poCurve) {
                std::cout << "x=" << poPoint.getX() << " ,";
                std::cout << "y=" << poPoint.getY() << std::endl;
            }
            std::cout << CurveNum << std::endl;  
        }
    }
    else {
        std::cout << poGeometry->getGeometryName() << std::endl;
        std::cout << poGeometry->getGeometryType() << std::endl;
        std::cout << "几何图案读取失败" << std::endl;
    }
    OGRFeature::DestroyFeature(poFeature);

    ////一个feature可能有多个Geometry
    //OGRGeometry* poGeometry;
    //int iGeomField;
    //int nGeomFieldCount;

    //nGeomFieldCount = poFeature->GetGeomFieldCount();
    //for (iGeomField = 0; iGeomField < nGeomFieldCount; iGeomField++)
    //{
    //    poGeometry = poFeature->GetGeomFieldRef(iGeomField);
    //    if (poGeometry != NULL
    //        && wkbFlatten(poGeometry->getGeometryType()) == wkbPoint)
    //    {
    //    #if GDAL_VERSION_NUM >= GDAL_COMPUTE_VERSION(2,3,0)
    //                OGRPoint* poPoint = poGeometry->toPoint();
    //    #else
    //                OGRPoint* poPoint = (OGRPoint*)poGeometry;
    //    #endif

    //        printf("%.3f,%3.f\n", poPoint->getX(), poPoint->getY());
    //    }
    //    else
    //    {
    //        printf("no point geometry\n");
    //    }
    //}

    //gdal3 c++ 11 不需要释放资源
    //OGRFeature::DestroyFeature(poFeature);
    //GDALClose(poDS);

}

//参考代码
// gdal2.3+ c++11
//{
//GDALAllRegister();
//
//GDALDatasetUniquePtr poDS(GDALDataset::Open("point.shp", GDAL_OF_VECTOR));
//if (poDS == nullptr)
//{
//    printf("Open failed.\n");
//    exit(1);
//}
//
//for (const OGRLayer* poLayer : poDS->GetLayers())
//{
//    for (const auto& poFeature : *poLayer)
//    {
//        for (const auto& oField : *poFeature)
//        {
//            if (oField.IsUnset())
//            {
//                printf("(unset),");
//                continue;
//            }
//            if (oField.IsNull())
//            {
//                printf("(null),");
//                continue;
//            }
//            switch (oField.GetType())
//            {
//            case OFTInteger:
//                printf("%d,", oField.GetInteger());
//                break;
//            case OFTInteger64:
//                printf(CPL_FRMT_GIB ",", oField.GetInteger64());
//                break;
//            case OFTReal:
//                printf("%.3f,", oField.GetDouble());
//                break;
//            case OFTString:
//                // GetString() returns a C string
//                printf("%s,", oField.GetString());
//                break;
//            default:
//                // Note: we use GetAsString() and not GetString(), since
//                // the later assumes the field type to be OFTString while the
//                // former will do a conversion from the original type to string.
//                printf("%s,", oField.GetAsString());
//                break;
//            }
//        }
//
//        const OGRGeometry* poGeometry = poFeature->GetGeometryRef();
//        if (poGeometry != nullptr
//            && wkbFlatten(poGeometry->getGeometryType()) == wkbPoint)
//        {
//            const OGRPoint* poPoint = poGeometry->toPoint();
//
//            printf("%.3f,%3.f\n", poPoint->getX(), poPoint->getY());
//        }
//        else
//        {
//            printf("no point geometry\n");
//        }
//    }
//}
//return 0;
//}

//旧版本int main()
//{
//GDALAllRegister();
//
//GDALDataset* poDS = static_cast<GDALDataset*>(
//    GDALOpenEx("point.shp", GDAL_OF_VECTOR, NULL, NULL, NULL));
//if (poDS == NULL)
//{
//    printf("Open failed.\n");
//    exit(1);
//}
//
//OGRLayer* poLayer = poDS->GetLayerByName("point");
//OGRFeatureDefn* poFDefn = poLayer->GetLayerDefn();
//
//poLayer->ResetReading();
//OGRFeature* poFeature;
//while ((poFeature = poLayer->GetNextFeature()) != NULL)
//{
//    for (int iField = 0; iField < poFDefn->GetFieldCount(); iField++)
//    {
//        if (!poFeature->IsFieldSet(iField))
//        {
//            printf("(unset),");
//            continue;
//        }
//        if (poFeature->IsFieldNull(iField))
//        {
//            printf("(null),");
//            continue;
//        }
//        OGRFieldDefn* poFieldDefn = poFDefn->GetFieldDefn(iField);
//
//        switch (poFieldDefn->GetType())
//        {
//        case OFTInteger:
//            printf("%d,", poFeature->GetFieldAsInteger(iField));
//            break;
//        case OFTInteger64:
//            printf(CPL_FRMT_GIB ",", poFeature->GetFieldAsInteger64(iField));
//            break;
//        case OFTReal:
//            printf("%.3f,", poFeature->GetFieldAsDouble(iField));
//            break;
//        case OFTString:
//            printf("%s,", poFeature->GetFieldAsString(iField));
//            break;
//        default:
//            printf("%s,", poFeature->GetFieldAsString(iField));
//            break;
//        }
//    }
//
//    OGRGeometry* poGeometry = poFeature->GetGeometryRef();
//    if (poGeometry != NULL
//        && wkbFlatten(poGeometry->getGeometryType()) == wkbPoint)
//    {
//        OGRPoint* poPoint = (OGRPoint*)poGeometry;
//
//        printf("%.3f,%3.f\n", poPoint->getX(), poPoint->getY());
//    }
//    else
//    {
//        printf("no point geometry\n");
//    }
//    OGRFeature::DestroyFeature(poFeature);
//}
//
//GDALClose(poDS);
//}