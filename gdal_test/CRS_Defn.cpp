#include "CRS_Defn.h"

void CRS_defn::addDefn(OGRLayer* tarLayer , OGRFieldDefn* newField) {
	if (tarLayer == NULL || newField == NULL) {
		std::cout << "输入无效" << std::endl;
		return;
	}
	if (tarLayer->CreateField(newField) == OGRERR_NONE) {
		std::cout << "新增属性列成功" << std::endl;
	}
}

void CRS_defn::deleteDefn(OGRLayer* tarLayer, int fieldIndex)
{
	if (tarLayer == NULL || fieldIndex < 0 || fieldIndex >= tarLayer->GetLayerDefn()->GetFieldCount()) {
		std::cout << "输入无效" << std::endl;
		return;
	}
	tarLayer->DeleteField(fieldIndex);
}

