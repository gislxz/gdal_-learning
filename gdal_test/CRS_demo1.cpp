#include "CRS_demo1.h"
//创建自定义空间参考对象
OGRSpatialReference* CreateMyCRS() {
    //创建OGRSpatialReference对象
    OGRSpatialReference* oSRS = new OGRSpatialReference;
    //设置坐标系
    oSRS->SetGeogCS("My geographic CRS",
        "World Geodetic System 1984",
        "My WGS84 Spheroid",
        SRS_WGS84_SEMIMAJOR, SRS_WGS84_INVFLATTENING,
        "Greenwich", 0.0,
        "degree", atof(SRS_UA_DEGREE_CONV));
    //重设成知名坐标系
    oSRS->SetWellKnownGeogCS("WGS84");
    //如果 EPSG 数据库可用，可用 GCS 代码编号设置。
    oSRS->SetWellKnownGeogCS("EPSG:4326");
    return oSRS;
}

//将OGRSpatialReference对象输出为WKT格式
void OGRSRtoWkt(OGRSpatialReference* oSRS){
    char* pszWKT = NULL;
    //oSRS->exportToWkt(&pszWKT);
    //printf("%s\n", pszWKT);
    //设置参数
    const char* apszOptions[] = { "FORMAT=WKT2_2018", "MULTILINE=YES", nullptr };
    oSRS->exportToWkt(&pszWKT, apszOptions);
    printf("%s\n", pszWKT);
    CPLFree(pszWKT);
}

void CreateMyUtm() {
    //创建空间参考对象
    OGRSpatialReference oSRS;

    //设置地理坐标系和投影坐标系
    oSRS.SetProjCS("UTM 17 (WGS84) in northern hemisphere.");
    //设置地理坐标系和投影坐标系
    oSRS.SetWellKnownGeogCS("WGS84");
    oSRS.SetUTM(17, TRUE);

    char* pszWKT = NULL;
    const char* apszOptions[] = { "FORMAT=WKT2_2018", "MULTILINE=YES", nullptr };
    oSRS.exportToWkt(&pszWKT, apszOptions);
    std::cout << pszWKT << std::endl;
    CPLFree(pszWKT);
}

//投影坐标转换
//void GetSRSInfo(OGRSpatialReference* poSRS) {
//    const char* pszProjection = poSRS->GetAttrValue("PROJECTION");
//
//    if (pszProjection == NULL)
//    {
//        if (poSRS->IsGeographic())
//            sprintf(szProj4 + strlen(szProj4), "+proj=longlat ");
//        else
//            sprintf(szProj4 + strlen(szProj4), "unknown ");
//    }
//    else if (EQUAL(pszProjection, SRS_PT_CYLINDRICAL_EQUAL_AREA))
//    {
//        sprintf(szProj4 + strlen(szProj4),
//            "+proj=cea +lon_0=%.9f +lat_ts=%.9f +x_0=%.3f +y_0=%.3f ",
//            poSRS->GetProjParm(SRS_PP_CENTRAL_MERIDIAN, 0.0),
//            poSRS->GetProjParm(SRS_PP_STANDARD_PARALLEL_1, 0.0),
//            poSRS->GetProjParm(SRS_PP_FALSE_EASTING, 0.0),
//            poSRS->GetProjParm(SRS_PP_FALSE_NORTHING, 0.0));
//    }
//}