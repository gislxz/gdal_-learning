﻿#include "GetRasterInformation.h"

void GetRasterInformation(const char* file)
{
    //注册文件格式
    GDALAllRegister();

    //使用只读方式打开图像
    GDALDataset* poDataset = (GDALDataset*)GDALOpen(file, GA_ReadOnly);
    if (poDataset == NULL)
    {
        cout << "File:" << "E:\\DataSet\\test.tif" << "不能打开！" << endl;
        return;
    }

    //输出图像格式信息
    cout << "Driver: " << poDataset->GetDriver()->GetDescription()
        << "/" << poDataset->GetDriver()->GetMetadataItem(GDAL_DMD_LONGNAME)
        << endl;

    //输出图像的大小和波段个数
    cout << "Size is " << poDataset->GetRasterXSize()
        << "x" << poDataset->GetRasterYSize()
        << "x" << poDataset->GetRasterCount()
        << endl;

    //输出图像的投影信息
    if (poDataset->GetProjectionRef() != NULL)
    {
        cout << "Projection is " << poDataset->GetProjectionRef() << endl;
    }

    //输出图像的坐标和分辨率信息
    double adfGeoTransform[6];
    if (poDataset->GetGeoTransform(adfGeoTransform) == CE_None)
    {
        printf("Origin = (%.6f,%.6f)\n", adfGeoTransform[0], adfGeoTransform[3]);
        printf("PixelSize = (%.6f,%.6f)\n", adfGeoTransform[1], adfGeoTransform[5]);
    }

    //读取第一个波段
    GDALRasterBand* poBand = poDataset->GetRasterBand(1);
    GDALDataType type = poBand->GetRasterDataType();//获取数据类型
    cout << "第一个波段的数据类型为：" << GDALGetDataTypeName(type) << endl;
    //获取该波段的最大值最小值，如果获取失败，则进行统计
    int bGotMin, bGotMax;
    double adfMinMax[2];
    adfMinMax[0] = poBand->GetMinimum(&bGotMin);
    adfMinMax[1] = poBand->GetMaximum(&bGotMax);

    if (!(bGotMin && bGotMax))
    {
        GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, adfMinMax);
    }

    printf("Min = %.3fd , Max = %.3f\n", adfMinMax[0], adfMinMax[1]);

    //关闭文件
    GDALClose(poDataset);
}
