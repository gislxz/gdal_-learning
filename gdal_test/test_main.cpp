// gdal_test.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <Windows.h>
#include "GetRasterInformation.h"
#include "CRS_demo1.h"
#include "OGRGeometry_demo1.h"
#include "CRS_Defn.h"


int func1(OGRFeature* poFeature) {
	string xianType = poFeature->GetFieldAsString(6);
	if (xianType == u8"市辖区")
		return 0;
	else if (xianType == u8"县")
		return 1;
	else if (xianType == u8"自治县")
		return 2;
	else
		return -1;
}

int main()
{
	//全局utf-8编码
	//system("chcp 65001");
	//gdal配置测试
	//const char* inFile = "E:\\test.tif";

	//GetRasterInformation(inFile);

	//system("pause");

	//创建自定义坐标系
	/*
	auto MyCRS = CreateMyCRS();
	cout << MyCRS->GetName() << endl;

	OGRSRtoWkt(MyCRS);

	delete MyCRS;
	*/

	//CreateMyUtm();

	//矢量数据基础操作测试
	//string chongqingDIR = "C:/Users/23005/Desktop/chongqing/chongqing.shp";

	//OpenESRIShp(chongqingDIR.c_str());

	//矢量数据属性列操作
	// 增加属性列
	GDALAllRegister();
	CPLSetConfigOption("GDAL_FILENAME_IS_UTF8", "NO");
	const char* chongqingDIR = "C:/Users/23005/Desktop/chongqing/重庆.shp";
	GDALDatasetUniquePtr poDS(GDALDataset::Open(chongqingDIR, GDAL_OF_VECTOR || GDAL_OF_UPDATE));
	if (poDS == NULL)
	{
		printf("Open failed.\n");
		exit(1);
	}
	OGRLayer* poLayer = poDS->GetLayer(0);

	OGRFieldDefn* testFieldDefn = new OGRFieldDefn("testStringDefn", OFTString);
	CRS_defn::addDefn(poLayer, testFieldDefn);
	delete testFieldDefn;

	//修改属性列的值
	int lastFieldIndex = poLayer->GetLayerDefn()->GetFieldCount() - 1;
	CRS_defn::assignDefn(poLayer, lastFieldIndex, [](OGRFeature* poFeature) {return u8"你好1"; });
	//CRS_defn::assignDefn(poLayer, lastFieldIndex, func1);

	//删除指定属性列
	//GDALAllRegister();
	//const char* chongqingDIR = "C:/Users/23005/Desktop/chongqing/chongqing.shp";
	//GDALDatasetUniquePtr poDS(GDALDataset::Open(chongqingDIR, GDAL_OF_VECTOR || GDAL_OF_UPDATE));
	//if (poDS == NULL)
	//{
	//    printf("Open failed.\n");
	//    exit(1);
	//}
	//OGRLayer* poLayer = poDS->GetLayer(0);
	//int lastFieldIndex = poLayer->GetLayerDefn()->GetFieldCount() - 1;
	//CRS_defn::deleteDefn(poLayer, lastFieldIndex);

	std::cout << "exit" << std::endl;
	return 0;
}


// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
